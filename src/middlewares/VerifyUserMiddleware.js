import UserDao from '../dao/UserDao';
import CommonResponse from '../dto/CommonResponse';
const jwt = require('jsonwebtoken');


require('dotenv').config();
const jwtSecret = process.env.jwt_secret;


const crypto = require('crypto');

exports.isDuplicateEmail = async (req, res, next) => {
    const user = await UserDao.getUserByEmail(req.body.email).catch((err) => {
        return next();
    });
    if(user){
        return res.status(400).send(new CommonResponse(40, `email ${req.body.email} already exist`))
    }
}

exports.hasAuthValidFields = (req, res, next) => {
    let errors = [];
    if (req.body) {
        if (!req.body.email) {
            errors.push('Missing email field');
        }
        if (!req.body.password) {
            errors.push('Missing password field');
        }

        if (errors.length) {
            return res.status(400).send(new CommonResponse(40, errors, {}));
        } else {
            return next();
        }
    } else {
        return res.status(400).send(new CommonResponse(40, "Missing email and password fields", {}));
    }
};

exports.isPasswordAndUserMatch = async (req, res, next) => {
    const user = await UserDao.getUserByEmail(req.body.email);
    if(user){
        let passwordFields = user.password.split('$');
        let salt = passwordFields[0];
        let hash = crypto.createHmac('sha512', salt).update(req.body.password).digest("base64");
        if(hash === passwordFields[1]){
            req.body = {
                firstName: user.firstName,
                lastName: user.lastName,
                email: user.email,
                dob: user.dob,
                date: new Date()
            }
            return next();
        }else {
            res.status(403).json(new CommonResponse(40, 'invalid email or password'))            
        }
    }else {
        res.status(403).json(new CommonResponse(40, 'invalid email or password'))
    }
    
};


exports.validJWTNeeded = (req, res, next) => {
    if (req.headers['authorization']) {
        try {
            let authorization = req.headers['authorization'].split(' ');
            if (authorization[0] !== 'Bearer') {
                return res.status(401).send(new CommonResponse(401, 'invalid token 1'));
            } else {
                req.jwt = jwt.verify(authorization[1], jwtSecret);
                return next();
            }
        } catch (err) {
            return res.status(403).send(new CommonResponse(401, 'invalid token 2'));
        }
    } else {
        return res.status(401).send(new CommonResponse(401, 'invalid token 3'));
    }
};