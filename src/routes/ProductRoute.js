import ProductController from '../controllers/ProductController';
import VerifyUserMiddleware from '../middlewares/VerifyUserMiddleware';

exports.routesConfig = function(app){
    app.post('/product', [
        VerifyUserMiddleware.validJWTNeeded,
        ProductController.saveProduct
    ])

    app.post('/products', [
        VerifyUserMiddleware.validJWTNeeded,
        ProductController.getAllProduct
    ])

    app.get('/product/:id', [
        VerifyUserMiddleware.validJWTNeeded,
        ProductController.getProductById
    ])

    app.put('/product', [
        VerifyUserMiddleware.validJWTNeeded,
        ProductController.updateProduct
    ])

    app.delete('/product/:id', [
        VerifyUserMiddleware.validJWTNeeded,
        ProductController.deleteProduct
    ])

    app.get('/product/more-than/:price', [
        VerifyUserMiddleware.validJWTNeeded,
        ProductController.getProductMoreThan
    ])
}
