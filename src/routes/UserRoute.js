import VerifyUserMiddleware from '../middlewares/VerifyUserMiddleware';
import UserController from '../controllers/UserController';

exports.routesConfig = function(app){
    app.post('/register', [
        VerifyUserMiddleware.isDuplicateEmail,
        UserController.register
    ])
}