import VerifyUserMiddleware from '../middlewares/VerifyUserMiddleware';
import AuthenticationController from '../controllers/AuthenticationController';

exports.routesConfig = function(app){
    app.post('/login', [
        VerifyUserMiddleware.hasAuthValidFields,
        VerifyUserMiddleware.isPasswordAndUserMatch,
        AuthenticationController.login
    ])
}