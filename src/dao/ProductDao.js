import { sequelize } from '../models';
const Product = require('../models').Product;
const { QueryTypes } = require('sequelize');


module.exports = {
    saveProduct: function(params){
        return new Promise(async(resolve, reject) => {
            const product = await Product.create(params).catch((err) => {
                reject('an error has ocured while saving data')
            })
            if(product){
                resolve(product)
            }
        })
    },
    updateProduct: function(params){
        return new Promise(async(resolve, reject) => {
            const updated = await Product.update({
                productName: params.productName,
                price: params.price
            },{
                where:{
                    id: params.id
                }
            }).catch((err) => {
                console.log(err)
                reject('an error has ocured while update data');
            })

            if(updated){
                resolve(updated)
            }
        })
    },
    getAllProduct: function(params){
        return new Promise( async (resolve, reject) => {
            let order = [];
            if(params.orderBy){
                order.push([params.orderBy, params.orderType == null ? 'desc' : params.orderType])
            }
            const product = await Product.findAndCountAll({
                order,
                limit: params.size,
                offset: (params.page-1) * params.size,               
            }).catch((err) => {
                reject('an error has ocured while getting data');
            });

            if(product){
                resolve(product);
            }
        })
    },
    getProductById: function(id){
        return new Promise(async(resolve, reject) => {
            const product = await Product.findByPk(id).catch((err) => {
                reject('an error has ocured while getting data');
            });

            if(!product){
                reject(`product id ${id} not found`)
            }else{
                resolve(product)
            }
        })
    },
    deleteById: function(id){
        return new Promise(async(resolve, reject) => {
            const deleted = await Product.destroy({
                where: {
                    id
                }
            }).catch((err) => {
                reject(`failed delete data id ${id}`);
            })
            if(deleted){
                resolve(deleted)
            }
        })
    },
    getProductMoreThan:  function(price){
        return new Promise( async (resolve, reject) => {
            let product =  await sequelize.query(`SELECT * FROM products where price > ${price}`, {
                type: QueryTypes.SELECT,
                model: Product,
                mapToModel: true 
              }).catch((err) => {
                  reject('an error occured while get data')
              });

             if(product.length > 0){
                 resolve(product)
             }else{
                 reject('data not found')
             }
        })
    }
}