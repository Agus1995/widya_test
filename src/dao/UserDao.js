const User = require('../models').User;

module.exports = {
    getUserByEmail: function(email){
        return new Promise(async(resolve, reject) => {
            const user = await User.findOne({
                where: {
                    email
                }
            }).catch((err) => {
                console.log(err)
                reject('error while get user')
            })
            if(!user){
                reject(`email ${email} not found`)
            }else {
                resolve(user)
            }
        })
    },
    saveUser: function(params){
        return new Promise( async (resolve, reject) => {
            const user = User.create(params).catch((err) => {
                reject('error while saving data')
            })
            if(user){
                resolve(user)
            }
        })
    }
}