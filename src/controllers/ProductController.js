import ProductDao from '../dao/ProductDao';
import CommonResponse from '../dto/CommonResponse';

exports.saveProduct = async (req, res) => {
    const product = await ProductDao.saveProduct(req.body).catch((err) => {
        res.status(400).send(new CommonResponse(40, err))
    })
    if(product){
        res.status(201).send(new CommonResponse('','', product))
    }
}

exports.getAllProduct = async (req, res) => {
    const product = await ProductDao.getAllProduct(req.body).catch((err) => {
        res.status(400).send(new CommonResponse(40, err))
    })
    if(product){
        res.status(201).send(new CommonResponse('','', product))
    }
}

exports.getProductById = async (req, res) => {
    const product = await ProductDao.getProductById(req.params.id).catch((err) => {
        res.status(400).send(new CommonResponse(40, err))
    })
    if(product){
        res.status(201).send(new CommonResponse('','', product))
    }
}

exports.updateProduct = async (req, res) => {
    const product = await ProductDao.updateProduct(req.body).catch((err) => {
        res.status(400).send(new CommonResponse(40, err))
    })
    if(product){
        res.status(201).send(new CommonResponse('','', product))
    }
}

exports.deleteProduct = async (req, res) => {
    const product = await ProductDao.deleteById(req.params.id).catch((err) => {
        res.status(400).send(new CommonResponse(40, err))
    })
    if(product){
        res.status(201).send(new CommonResponse('','', product))
    }
}

exports.getProductMoreThan = async (req, res) => {
    const product = await ProductDao.getProductMoreThan(req.params.price).catch((err) => {
        res.status(400).send(new CommonResponse(44, err))
    })
    if(product){
        res.status(201).send(new CommonResponse('','', product))
    }
}