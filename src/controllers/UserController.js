import Userdao from '../dao/UserDao';
import CommonResponse from '../dto/CommonResponse';
const crypto = require('crypto');


exports.register = async (req, res) => {
    let salt = crypto.randomBytes(16).toString('base64');
    let hash = crypto.createHmac('sha512', salt).update(req.body.password).digest("base64");
    req.body.password = salt + "$" + hash;
    const user = await Userdao.saveUser(req.body).catch((err) => {
        res.status(400).send(new CommonResponse(40, err));
    });
    if(user){
        res.json(new CommonResponse(null, 'Registered'))
    }
}