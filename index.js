import Express from 'express';
import UserRoute from './src/routes/UserRoute';
import AuthenticationRoute from './src/routes/AuthenticationRoute';
import ProductRoute from './src/routes/ProductRoute';
import bodyParser from 'body-parser';

const dotenv = require('dotenv');
dotenv.config();
const app = Express();
const port = process.env.PORT;

app.use(function (req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Credentials', 'true');
    res.header('Access-Control-Allow-Methods', 'GET,HEAD,PUT,PATCH,POST,DELETE');
    res.header('Access-Control-Expose-Headers', 'Content-Length');
    res.header('Access-Control-Allow-Headers', 'Accept, Authorization, Content-Type, X-Requested-With, Range');
    if (req.method === 'OPTIONS') {
        return res.sendStatus(200);
    } else {
        return next();
    }
});

app.use(bodyParser.urlencoded({ limit: '30mb', extended: true }));
app.use(bodyParser.json({limit: '30mb', extended: true}));

UserRoute.routesConfig(app);
AuthenticationRoute.routesConfig(app);
ProductRoute.routesConfig(app);


app.listen(port, () => console.log(`App listening on port ${port}`));